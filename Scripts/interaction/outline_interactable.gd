extends Interactable

@export var mesh_instance :MeshInstance3D
@export var value:float =.1
var mat_next : ShaderMaterial
var is_ready:bool = false
func _ready():
	if mesh_instance.get_surface_override_material(0):
		mat_next = mesh_instance.get_surface_override_material(0).next_pass
		is_ready = true

func _hover():
	if is_ready:
		mat_next.set_shader_parameter("outline_thickness",value)

func _unhover():
	if is_ready:
		mat_next.set_shader_parameter("outline_thickness",0)
