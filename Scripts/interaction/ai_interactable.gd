extends Interactable

@export var state_machine:StateMachine
@export var on_interaction_state:State.States

var distance:float=2

func _interact():
	
	if _within_distance():
		state_machine._switch_state(on_interaction_state)

func _within_distance():
	return true
