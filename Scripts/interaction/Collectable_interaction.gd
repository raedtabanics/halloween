extends Interactable
@export var parent:Node3D
@export var _name:String
@export var _desc:String
@export var _url:String
# Called when the node enters the scene tree for the first time.
var timer:Timer
var used:bool=false

func _ready():
	timer = get_node("Timer")
	timer.wait_time =.2
func _interact():
	if used:
		return
	used = true
	timer.start()
	await timer.timeout
	Events.emit_signal("on_item_collected",ItemModel.new(_name,_desc,_url))
	parent.visible = false
	
