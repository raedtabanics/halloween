extends Interactable

@export var on_animation:String
@export var off_animation:String

var animator:AnimationPlayer
var state:bool = true

func _ready():
	animator = get_node("AnimationPlayer")
func _interact():
	if animator.animation_finished:
		if state:
			animator.play(off_animation)
			$Open_Coffin_SFX.play()
			state = false
		else:
			animator.play(on_animation)
			$Close_Coffin_SFX.play()
			state = true
		
