extends Interactable

@export var animation:String
var animator:AnimationPlayer
# Called when the node enters the scene tree for the first time.
func _ready():
	animator = get_node("AnimationPlayer")


func _interact():
	if animator.animation_finished:
		animator.play(animation)
		$Boing_3.play()
