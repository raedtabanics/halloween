extends Node

@export var parent:CharacterBody3D
@export var animation_tree:AnimationTree
@export var locomotion_path:String

var MAX_SPEED:float = 6
var last_position:Vector3
func _ready():
	last_position = parent.global_position
	#MAX_SPEED = parent.get_meta("RunSpeed")
	pass

func _physics_process(delta):
	var velocity = parent.global_position.distance_to(last_position)
	var current_speed = abs(velocity)/delta
	animation_tree.set(locomotion_path,current_speed/MAX_SPEED)
	last_position = parent.global_position
