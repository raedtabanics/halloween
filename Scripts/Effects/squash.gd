extends Node

@export var parent: CharacterBody3D
@export var scale_factor:float = .02
@export var lerp_speed: float = 10  

var original_y_scale:float
var original_x_scale:float
# Called when the node enters the scene tree for the first time.

func _ready():
	original_x_scale = parent.scale.x
	original_y_scale = parent.scale.y
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	_squish(parent.velocity.y,delta)
	

func _squish(y,delta):
	var target_scale_y = original_y_scale + abs(y) * scale_factor
	var target_scale_x = original_x_scale - abs(y) * scale_factor * 0.5

	# Use lerp to smoothly interpolate the scale
	parent.scale.y = lerp(parent.scale.y, target_scale_y, lerp_speed * delta)
	parent.scale.x = lerp(parent.scale.x, target_scale_x, lerp_speed * delta)

