extends Node

@export var camera:Camera3D

@export var force:float = .2
@export var fade:float = 10
var shake_strength=0
var rand = RandomNumberGenerator.new()
# Called when the node enters the scene tree for the first time.
func _ready():
	Events.on_jump_complete.connect(_apply_shake)
	pass # Replace with function body.

func _apply_shake(position):
	shake_strength = force
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	
	if shake_strength>0:
		shake_strength = lerpf(shake_strength,0,delta*fade)
		camera.h_offset = _random_offset()
		camera.v_offset = _random_offset()
	
func _random_offset() ->float:
	return rand.randf_range(-shake_strength,shake_strength)
