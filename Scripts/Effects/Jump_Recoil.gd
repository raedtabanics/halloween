extends Node

@export var parent:Node3D
const distance:float = 5
# Called when the node enters the scene tree for the first time.
func _ready():
	Events.on_jump_complete.connect(_recoil)

func _recoil(position):
	if abs(parent.global_position.distance_to(position)) < distance:
		var tween = create_tween()
		var original_position = parent.global_position
		tween.tween_property(parent,"position",parent.global_position + Vector3(0,.5,0),.1 )
		tween.tween_property(parent,"position",parent.global_position - Vector3(0,.5,0),.1 )
		await tween.finished
		parent.global_position = original_position
		
