extends Camera3D

var start = 65  # Starting value
var end = 75  # Ending value
var speed = 2.0  # Speed of change
var x = 0
var direction = 1  # 1 for increasing, -1 for decreasing

# Called when the node enters the scene tree for the first time.
func _ready():
	x = start


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	_focal(delta)
func _focal(delta):
	x += direction * speed * delta
	
	# Check if we've reached the end values
	if (x >= end and direction == 1) or (x <= start and direction == -1):
		direction *= -1  # Reverse the direction when reaching the end values
	
	# Ensure that the current_x stays within the specified range
	x = clamp(x, start, end)
	fov = x
	
	print(x)
	# Use the current_x value for whatever you need (e.g., updating a variable or property)
	# For example, you can print it to the console here
