extends MeshInstance3D


var y_start = 1.2  # Starting value
var y_end = 1.5   # Ending value
var speed = .5  # Speed of change
var current_y = 0
var direction = 1  # 1 for increasing, -1 for decreasing

func _process(delta):
	current_y += direction * speed * delta
	
	# Check if we've reached the end values
	if (current_y >= y_end and direction == 1) or (current_y <= y_start and direction == -1):
		direction *= -1  # Reverse the direction when reaching the end values
	
	# Ensure that the current_x stays within the specified range
	current_y = clamp(current_y, y_start, y_end)
	global_position.y = current_y
	
	
