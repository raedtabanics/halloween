extends ColorRect

var scene = preload("res://Maps/Map1.tscn")
# Called when the node enters the scene tree for the first time.
func _ready():
	_fade_in()


# Called every frame. 'delta' is the elapsed time since the previous frame.

func _fade_in():
	var tween = create_tween()
	tween.tween_property(self,"color",Color.TRANSPARENT,2)
	
func _fade_out():
		var tween = create_tween()
		tween.tween_property(self,"color",Color.BLACK,2)
		#tween.connect("_load_scene",tween.finished)
		await tween.finished
		_load_scene()
func _load_scene():
	get_tree().change_scene_to_packed(scene)


func _on_button_pressed():
	_fade_out()
