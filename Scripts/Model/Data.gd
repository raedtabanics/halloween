extends Node

const dialouges_data_path :String = "res://packs/data/dialouge.json"
const items_data_path:String ="res://packs/data/items.json"
const quests_data_path:String="res://packs/data/quests.json"

var dialouges :Dictionary
var items:Dictionary
var quests:Dictionary

# Called when the node enters the scene tree for the first time.
func _ready():
	dialouges = _load_data(dialouges_data_path)
	items = _load_data(items_data_path)
	quests = _load_data(quests_data_path)
	

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _load_data(path):
	if FileAccess.file_exists(path):
		var data_file = FileAccess.open(path,FileAccess.READ)
		var parsed_result =JSON.parse_string(data_file.get_as_text()) 
		
		if parsed_result is Dictionary:
			return parsed_result
		else:
			print("No dictionary is found")
	else:
		print("File doesn't exist")


func _load(id:String,progress:int):
	if Data.dialouges.has(id):
		if Data.dialouges[id].has(str(progress)):
			return Data.dialouges[id][str(progress)]["conversation"]
		else:
			print("Progress not found")
			return ""
	else:
		print("ID not found")
		return ""
