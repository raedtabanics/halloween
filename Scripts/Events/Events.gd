extends Node

signal on_notification(msg)
signal on_dialouge(_name,url,msg:Array)
signal on_jump_complete(position)
signal on_dialouge_commplete()
signal on_item_collected(item:ItemModel)
signal on_inventory_update()
signal on_cutscene_start()
signal on_cutscene_end()
signal on_quest_update()
signal on_quest_progress()

signal on_game_start()
signal on_tutorial(index:int)

enum Event_Type{
	QUEST,
	GAME_START,
	TUTORIAL
}

func _process_event_token(token:Event_Token):
	match token.type:
		Event_Type.QUEST:
			assert(token.param != "")
			emit_signal("on_quest_progress",token.param)
		Event_Type.GAME_START:
			emit_signal("on_game_start")
			print("Game started")
		Event_Type.TUTORIAL:
			emit_signal("on_tutorial",3)
