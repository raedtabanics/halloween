extends Node

var parent : Node3D
var run_speed:float
var walk_speed:float
var crouch_speed:float
# Called when the node enters the scene tree for the first time.
func _ready():
	parent = get_owner()
	run_speed = parent.get_meta("RunSpeed")
	walk_speed = parent.get_meta("WalkSpeed")
	crouch_speed = parent.get_meta("CrouchSpeed")
	
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if Input.is_action_pressed("ui_shift"):
		parent.set_meta("Speed",run_speed)
	elif Input.is_action_pressed("ctrl"):
		parent.set_meta("Speed",crouch_speed)
	else:
		parent.set_meta("Speed",walk_speed)
	pass
