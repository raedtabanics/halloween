extends Node

@export var character:CharacterBody3D
@export var animator:AnimationTree
@export var locomotion_param_path:String

var velocity:Vector3
var last_position:Vector3 = Vector3(0,0,0)

@export var max_speed:float=1
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	_calculate_velocity()
	print(velocity.length()*10)
	_locomotion(delta)
	last_position = character.global_position
	
func _locomotion(delta):
	animator.set(locomotion_param_path,velocity.length()/(max_speed * delta))

func _calculate_velocity():
	var displacment = character.global_position.direction_to(last_position)

	if displacment.length() <.1:
		velocity = Vector3(0,0,0)
	else:
		velocity = last_position - character.global_position
