extends Node

var parent : Node3D
@export var animator:AnimationPlayer
@export var lerp_speed:float =3
var current_speed:float = 0
var rotation_speed:float = 25


var is_ready:bool=true
# Called when the node enters the scene tree for the first time.
func _ready():
	parent = get_owner()
	Events.on_cutscene_start.connect(func():is_ready=false)
	Events.on_cutscene_end.connect(func():is_ready=true)


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	if not is_ready:
		animator.play("Idle")
		return
		
	var direction = _read_input()
	var target_speed = parent.get_meta("Speed")
	current_speed = lerp(current_speed,target_speed,lerp_speed*delta)
	
	_move(direction,current_speed * delta)
	_animate(direction)
	if direction:
		_look_direction(direction,delta)

func _read_input():
	var direction : Vector2 = Vector2(0,0)
	if Input.is_action_pressed("ui_left"):
		direction.x +=.1
	if Input.is_action_pressed("ui_right"):
		direction.x -=.1
	if Input.is_action_pressed("ui_up"):
		direction.y +=.1
	if Input.is_action_pressed("ui_down"):
		direction.y -=.1
	return direction
	
func _move(direction, speed):
	parent.global_position += Vector3(direction.x,0,direction.y).normalized() * speed
	
func _look_direction(direction,delta):
	var angle = atan2(direction.x,direction.y)
	var rotation = parent.rotation
	rotation.y = lerpf(rotation.y,angle,delta * rotation_speed)
	parent.rotation= rotation

func _animate(direction:Vector2):
	if direction.length() ==0:
		animator.play("Idle")
	elif current_speed > parent.get_meta("WalkSpeed"):
		animator.play("Run")
	else:
		animator.play("Walk")
