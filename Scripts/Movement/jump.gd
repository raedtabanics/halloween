extends Node3D

@export var parent: CharacterBody3D
@export var force: float
@export var force_double: float
@export var gravity: float

var velocity: Vector3 = Vector3(0, 0, 0)
var has_jumped: bool = false
var jumps_left: int = 1  # Number of jumps allowed (1 double jump)
var mask: int

var is_ready:bool=true
func _ready():
	Events.on_cutscene_start.connect(func():is_ready=false)
	Events.on_cutscene_end.connect(func():is_ready=true)
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	if not is_ready:
		return
		
	if has_jumped:
		if parent.is_on_floor():
			print("Jump End")
			has_jumped = false
			Events.emit_signal("on_jump_complete",parent.global_position)

	if Input.is_action_just_pressed("space"):
		
		if parent.is_on_floor() or jumps_left > 0:
			if parent.is_on_floor():
				jumps_left = 2  # Reset the number of jumps on the ground
			
			if jumps_left ==1:
				print("Double")
				parent.velocity.y = parent.velocity.y + force_double 
			else:
				print("Single")
				parent.velocity.y = force
			has_jumped = true
			jumps_left -= 1

	_update(delta)

func _jump():
	pass

func _update(delta):
	if not parent.is_on_floor():
		if parent.velocity.y <= 0:
			parent.velocity.y = parent.velocity.y + gravity * delta * 3
		else:
			parent.velocity.y = parent.velocity.y + gravity * delta
	parent.velocity = Vector3(0,parent.velocity.y,0)
	parent.move_and_slide()
