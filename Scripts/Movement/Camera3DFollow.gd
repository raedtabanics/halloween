extends Node

@export var camera:Camera3D
@export var target:Node3D
@export var offset:Vector3
@export var lerp_speed:float


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	var target_position = target.position + offset
	camera.position = lerp(camera.position,target_position,delta * lerp_speed)
	
