extends Resource
class_name Conditional

enum Type{
	QUEST,INVENTORY
}

@export var type:Type
@export var reference:Node
@export var value:bool
