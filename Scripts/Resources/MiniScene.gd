extends Resource
class_name Mini_Scene

@export var position:Vector3
@export var rotation:Vector3 
@export var camera_transform:Node3D
@export var animator:AnimationTree
@export var animation_param_path:String=""
@export var animation_id:String=""
@export var dialouge_id:String = ""
@export var dialouge_name:String=""
@export var dialouge_url:String=""
@export var dialouge_progress:int
@export var sfx:AudioStreamWAV

