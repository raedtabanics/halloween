extends Node

@export var state_machine:StateMachine
@export var target : Node3D
@export var parent : Node3D

@export var on_sight:State.States


@export var distance : float
@export var angle :float


func _process(delta):
	if target == null:
		return
	if _is_visible():
		state_machine._switch_state(on_sight)
	
	
func _is_visible():
	return _is_within_distance() and _is_within_angle()
	
func _is_within_distance():
	if (parent.position.distance_to(target.position) < distance):
		return true
	else:
		return false

func _is_within_angle():
	var to_target = target.position - parent.position
	var z = rad_to_deg(parent.basis.z.angle_to(to_target))
	
	if z <= angle :
		return true
	else:
		return false
	
