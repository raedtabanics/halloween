extends ColorRect


# Called when the node enters the scene tree for the first time.
func _ready():
	_fade_in()


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func _fade_in():
	var tween = create_tween()
	tween.tween_property(self,"color",Color.TRANSPARENT,1).set_ease(Tween.EASE_IN_OUT)

func _fade_out():
	var tween = create_tween()
	tween.tween_property(self,"color",Color.BLACK,.5).set_ease(Tween.EASE_IN_OUT)
