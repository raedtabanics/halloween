extends Control

var panel_1:NinePatchRect
var icon:TextureRect
var text:RichTextLabel

var panel_2:NinePatchRect

@export var i_key:Texture2D
@export var q_key:Texture2D

# Called when the node enters the scene tree for the first time.
func _ready():
	panel_1 = $NinePatchRect
	panel_2 = $Movement
	icon = $NinePatchRect/TextureRect
	text = $NinePatchRect/RichTextLabel
	
	Events.on_tutorial.connect(_show)


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _show(index):
	match index:
		1:
			icon.texture = i_key
			text.text = "[center]You can access/exit Inventory with I"
			_show_hide(panel_1,2)
		2:
			icon.texture = q_key
			text.text = "[center]You can access/exit Quests with Q"
			_show_hide(panel_1,2)
		3:
			_show_hide(panel_2,4)

func _show_hide(panel,delay):
	var tween = create_tween()
	tween.tween_property(panel,"offset_left",0,.4).set_ease(Tween.EASE_OUT)
	tween.tween_property(panel,"offset_right",0,.4).set_ease(Tween.EASE_OUT)
	tween.tween_property(panel,"offset_left",-900,.4).set_ease(Tween.EASE_OUT).set_delay(delay)
	tween.tween_property(panel,"offset_right",-900,.4).set_ease(Tween.EASE_OUT)
