extends Control

var text:RichTextLabel
var timer:Timer

@export var text_speed:float =.04
@export var delay:float =1

var loading:bool = false
# Called when the node enters the scene tree for the first time.
func _ready():
	Events.on_notification.connect(_show)
	text = get_node("RichTextLabel")
	timer = get_node("Timer")
	
	_hide()


func _show(msg):
	if loading:
		return
	loading = true
	
	text.text = msg
	text.visible_characters = 0
	
	var tween = create_tween()
	tween.tween_property(self,"scale",Vector2(1,1),.1)
	
	timer.wait_time = text_speed
	while text.visible_characters < len(text.text):
		text.visible_characters +=1
		timer.start()
		await timer.timeout
	
	timer.wait_time = delay
	timer.start()
	await timer.timeout
	_hide()
	loading = false
	
func _hide():
	var tween = create_tween()
	tween.tween_property(self,"scale",Vector2(0,0),.1)

