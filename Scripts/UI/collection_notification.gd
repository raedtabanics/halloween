extends Control

var text:RichTextLabel
var timer:Timer

@export var text_speed:float =.02
@export var delay:float =.7

var loading:bool = false
var image:TextureRect
# Called when the node enters the scene tree for the first time.
func _ready():
	Events.on_item_collected.connect(_show)
	text = get_node("RichTextLabel")
	timer = get_node("Timer")
	image = get_node("TextureRect")
	_hide()


func _show(item:ItemModel):
	if loading:
		return
	loading = true
	
	text.text = "[center] you have collected [color=green][wave]"+item._name+"[/wave][/color]"+".[/center]"
	text.visible_characters = 0
	
	var img = Image.new()
	img.load(item._url)
	
	if true:
		print("Image found")
		var texture = ImageTexture.new()
		texture.create_from_image(img)
		print(img)
		print(texture)
		image.texture = load(item._url)
		
	var tween = create_tween()
	tween.tween_property(self,"scale",Vector2(1,1),.1)
	
	timer.wait_time = text_speed
	while text.visible_characters < len(text.text):
		text.visible_characters +=1
		timer.start()
		await timer.timeout
	
	timer.wait_time = delay
	timer.start()
	await timer.timeout
	_hide()
	loading = false
	
func _hide():
	var tween = create_tween()
	tween.tween_property(self,"scale",Vector2(0,0),.1)

