extends Control

@export var text_speed =0.04

var text:RichTextLabel
var _label:RichTextLabel
var timer:Timer
var player:AudioStreamPlayer

var loading:bool = false
var finished:bool = true

var current:Array
var _name:String
var index:int=0


var left:TextureRect
var right:TextureRect

func _ready():
	Events.on_dialouge.connect(_load)
	
	text = get_node("Msg")
	_label = get_node("Name")
	timer = get_node("Timer")
	player = get_node("AudioStreamPlayer")
	left = get_node("Left_Avatar")
	right = get_node("Right_Avatar")
	
	timer.wait_time = text_speed
	_hide()

func _finish():
	text.visible_characters = len(text.text)-1
	finished = true
	pass

func _process(delta):
	if not loading:
		return
		
	if Input.is_action_just_pressed("click"):
		if finished:
			_next()
		else:
			_force_complete()		
	
func _load(_name,url,msg):
	if loading:
		return
	print(_name)
	print(msg)
	loading = true
	current = msg
	_label.text ="[wave]"+_name+"[/wave]"
	index =0
	
	if url !="":
		_load_pic(url,true)
	
	_show()
	_next()
	
func _load_pic(url,is_left:bool):
	if is_left:
		left.texture = load(url)
	else:
		right.texture = load(url)
		
func _next():
	if index>=current.size():
		_end()
		return
	text.text = current[index]
	text.visible_characters =0
	finished = false
	
	while text.visible_characters < len(current[index]):
		text.visible_characters+=1
		player.play(0)
		timer.start()
		await timer.timeout

	finished = true
	index+=1
	
func _end():
	loading = false
	Events.emit_signal("on_dialouge_commplete")
	_hide()
	
func _force_complete():
	text.visible_characters = len(current[index])
	
	
func _show():
	var tween = create_tween()
	tween.tween_property(self,"scale",Vector2(1,1),.3)
	
	
func _hide():
	var tween = create_tween()
	tween.tween_property(self,"scale",Vector2(0,0),.2)

