extends Node


var available:Dictionary
var current:Dictionary
var completed:Dictionary

var mission:String
var first_time_used:bool = true
func _ready():
	available = Data.quests
	Events.on_quest_progress.connect(_is_avaialble)
	for item in available:
		print(item)

func _process(delta):
	if Input.is_action_just_pressed("ctrl"):
		Events.emit_signal("on_quest_progress","001")
		
func _is_avaialble(id:String):
	print("we are here we quest"+id)
	if available.has(id):
		mission = available[id]["desc"]
		if first_time_used:
			first_time_used = false
			Events.emit_signal("on_tutorial",2)
		_start_quest(id)
	elif current.has(id):
		_on_quest_complete(id)
			
func _start_quest(id):
	var dictionary_item = available[id]
	available.erase(id)
	for item in available:
		print(item)
	current[id]= dictionary_item
	var quest = Quest.new(id,dictionary_item)
	quest.on_complete.connect(_on_quest_complete)
	Events.emit_signal("on_quest_update")
	
func _on_quest_complete(id):
	var dictionary_item = current[id]
	current.erase(id)
	completed[id] = dictionary_item
	mission = "Explore Halloween Town...!"
	Events.emit_signal("on_quest_update")
	


	
