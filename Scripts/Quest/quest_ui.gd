extends Control

var current:ItemList
var completed:ItemList
var text:RichTextLabel
var timer:Timer

@export var progress_Icon:Texture2D
@export var complete_Icon:Texture2D

@export var panel:Control
@export var top_panel:NinePatchRect
const textSpeed:float = .02

var status:bool=false
func _ready():
	current =get_node("NinePatchRect2/ItemListCurrent")
	completed = get_node("NinePatchRect2/ItemListCompleted")
	text = get_node("NinePatchRect/RichTextLabel")
	timer = get_node("Timer")
	
	timer.wait_time = textSpeed
	Events.on_game_start.connect(_display)
	Events.on_quest_update.connect(_update)
	
func _display():
	var tween = create_tween()
	tween.tween_property(top_panel,"offset_top",0,.4)
	tween.tween_property(top_panel,"offset_bottom",0,.3)
	
func _process(delta):
	if Input.is_action_just_pressed("Q"):
		if status:
			_hide()
		else:
			_show()
		status = !status
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _update():
	current.clear()
	completed.clear()
	_populate_list(QuestSystem.current,current,progress_Icon)
	_populate_list(QuestSystem.completed,completed,complete_Icon)
	
	_update_current_mission()

func _update_current_mission():
	text.text = "[center]"+QuestSystem.mission+"[/center]"
	text.visible_characters = 0
	while text.visible_characters < len(text.text):
		text.visible_characters +=1
		timer.start()
		await timer.timeout
			
	
func _populate_list(listFrom,listTo:ItemList,icon):
	for item in listFrom:
		print(item+"F")
		listTo.add_item(listFrom[item]["title"],icon,false)
		

func _show():
	var tween= create_tween()
	tween.tween_property(panel,"scale",Vector2(1,1),.3)
	

func _hide():
	print("Hiding Quest")
	var tween= create_tween()
	tween.tween_property(panel,"scale",Vector2(0,0),.3)
