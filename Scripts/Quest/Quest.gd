extends Node
class_name Quest

signal on_complete(quest)

var id:String
var title:String
var desc:String
var progress:int
var reward

func _init(id,data):
	self.id = id
	title = data.title
	desc = data.desc
	progress = data.progress
	reward = data.reward
	
func _ready():
	Events.on_quest_progress.connect(_on_progress)
	
func _process(delta):
		if Input.is_action_just_pressed("ctrl"):
			Events.emit_signal("on_quest_progress",id)
			print("HELLO")
	
func _on_progress(id):
	print("ARE WE HERE")
	if self.id !=id:
		return
	Events.emit_signal("on_quest_progress",id)

