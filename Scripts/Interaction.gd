extends Node3D

@export var camera : Camera3D
@export var mask = 1<<4
const LENGTH = 100
var old_result

const distance:float = 3
var is_ready:bool=true

@export var default_curser:Texture2D
@export var hover_curser:Texture2D

# Called when the node enters the scene tree for the first time.
func _ready():
	Events.on_cutscene_start.connect(func():is_ready=false)
	Events.on_cutscene_end.connect(func():is_ready=true)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	if not is_ready:
		return
		
	_unhover(old_result)
	var result = _cast_ray()
	
	if not result:
		_set_mouse_courser(false)
		return
	if not _is_within_interaction_distance(result):
		return
	
	_set_mouse_courser(true)
	_invoke_hover(result)
	if Input.is_action_just_pressed("click"):
		_invoke_interaction(result)
		
	if result != old_result:
		old_result= result
		
func _cast_ray():
	var space_state = get_world_3d().direct_space_state
	var mouse_position = get_viewport().get_mouse_position()
	
	var origin = camera.project_ray_origin(mouse_position)
	var end = origin + camera.project_ray_normal(mouse_position) * LENGTH
	
	var query = PhysicsRayQueryParameters3D.create(origin, end,mask )
	query.collide_with_areas = true
	return space_state.intersect_ray(query)
	
func _is_within_interaction_distance(result):
	if global_position.distance_to(result.collider.global_position) <=distance:
		return true
	return false
	
func _invoke_interaction(result):
	for child in result.collider.get_children():
		if child is Interactable:
			child._interact()

func _invoke_hover(result):
	for child in result.collider.get_children():
		if child is Interactable:
			child._hover()

func _unhover(result):
	if not result:
		return
	for child in result.collider.get_children():
		if child is Interactable:
			child._unhover()
	pass

func _set_mouse_courser(value:bool):
	if value:
		Input.set_custom_mouse_cursor(hover_curser)
	else:
		Input.set_custom_mouse_cursor(default_curser)
