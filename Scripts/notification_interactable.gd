extends Interactable

@export var id:String
# Called when the node enters the scene tree for the first time.
func _interact():
	var msg =_load()
	Events.emit_signal("on_notification",msg)

func _load():
	if Data.items.has(id):
			return Data.items[id]["Comment"]
	else:
		print("ID not found")


