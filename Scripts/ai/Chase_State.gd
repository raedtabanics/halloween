extends State

@export var character:CharacterBody3D
@export var state_machine:StateMachine
@export var path_finding:NavigationAgent3D
@export var target:Node3D

@export var speed:float
@export var distance:float
@export var offset:float=1

@export var off_sight:State.States
@export var on_reach:State.States

func _update():
	if _is_within_distance():
		if target.global_position.distance_to(character.global_position) > offset: 
			_move()
	else:
		state_machine._switch_state(off_sight)

func _enter():
	pass

func _is_within_distance():
	if (target.global_position.distance_to(character.global_position) <=distance):
		return true
	return false

func _move():
	path_finding.target_position = target.global_position
	
	var position = character.global_position
	var destination = path_finding.get_next_path_position()
	var velocity = (destination - position).normalized() * speed
	
	character.velocity = velocity
	character.move_and_slide()
