extends Node

@export var character:CharacterBody3D
@export var animator:AnimationTree
@export var mesh:Node3D
var rotation_speed:float = 25
@export var locomotion_param_path:String
@export var max_speed:float
var last_position:Vector3 =Vector3(0,0,0)
var gravity:float = -9
var velocity:Vector3 = Vector3(0,0,0)
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	_locomotion()
	_look_direction(delta)
	#_apply_gravity(delta)
	
	last_position= character.global_position
	
func _locomotion():
	var current_position = character.global_position
	var displacment = current_position.direction_to(last_position)
	#var velocity
	if displacment.length() <.1:
		velocity = Vector3(0,0,0)
	else:
		velocity = character.get_real_velocity()
	
	
	animator.set(locomotion_param_path,velocity.length()/max_speed)
	
	
func _look_direction(delta):
	#var direction = character.velocity.normalized()
	var direction = velocity
	if( abs(direction.length())<.1):
		return
	var target_rotation = atan2(direction.x,direction.z)
	var current_rotation = mesh.global_rotation.y
	var new_rotation = lerp_angle(current_rotation, target_rotation, delta * rotation_speed)
	mesh.global_rotation.y= new_rotation
	#character.rotate_object_local(Vector3(0, angle - character.rotation.y, 0) , delta * rotation_speed)
	#character.rotate_y(angle)
func _apply_gravity(delta):
	if not character.is_on_floor():
		character.velocity.y = character.velocity.y + gravity * delta
		#character.move_and_slide()
	
