extends Node
class_name  StateMachine

@export var parent:CharacterBody3D


var current
var patrol
var interaction
var chase


# Called when the node enters the scene tree for the first time.
func _ready():
	patrol = get_node("Patrol")
	interaction = get_node("interaction_state")
	chase = get_node("Chase_State")
	

	current = get_node("Patrol")
	current._enter()



# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	current._update()

func _switch_state(to_state):

	match to_state:
		State.States.PATROL:
			current = patrol
		State.States.ATTACK:
			pass
		State.States.CHASE:
			current = chase
		State.States.INTERACTION:
			current = interaction
	current._enter()

