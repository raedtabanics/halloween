extends State
@export var is_active:bool = false
@export var character:CharacterBody3D
@export var state_machine:StateMachine
@export var path_finding:NavigationAgent3D
@export var speed:float

@export var points:Array[Node3D]

enum Strategy{FIXED,RANDOM,DYNAMIC}
@export var stratgey:Strategy = Strategy.FIXED



var index:int = 0
var target:Node3D
var rand = RandomNumberGenerator.new()
var distance:float = 1
@onready var timer:Timer = get_node("Timer")
var delay:float = 2
var is_ready:bool=true

func _update():
	if not is_active:
		return
	
	if not is_ready:
		return
	
	if abs(character.global_position.distance_to(target.global_position)) <= distance :
		_next_target()
	_move()

func _enter():
	if not is_active:
		return
	timer.wait_time = delay
	_next_target()
	
func _next_target():
	is_ready = false
	
	match stratgey:
		Strategy.RANDOM:
			target = points[rand.randi_range(0,points.size()-1)]
		Strategy.FIXED:
			target = points[index]
			index = (index + 1) % points.size() 
			
	path_finding.target_position = target.global_position
	character.velocity = Vector3(0,0,0)
	timer.start()
	await timer.timeout
	is_ready = true
func _move():
	var position = character.global_position
	var destination = path_finding.get_next_path_position()
	var velocity = (destination - position).normalized() * speed
	
	character.velocity = velocity
	character.move_and_slide()
