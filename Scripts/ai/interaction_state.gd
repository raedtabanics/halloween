extends State

@export var state_machine:StateMachine
@export var to_state:State.States
@onready var timer:Timer= get_node("Timer")

func _ready():
	timer.wait_time = 10
	pass
# Called when the node enters the scene tree for the first time.
func _update():
	
	pass

func _enter():
	timer.start()
	await timer.timeout
	_exit()
func _exit():
	state_machine._switch_state(to_state)
