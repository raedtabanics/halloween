extends Node3D

@export var scenes:Array[Mini_Scene]
@export var prerequisite:String=""
@export var fade:ColorRect
@export var player:AudioStreamPlayer
var used:bool = false

signal test_signal
var main_cam:Camera3D
@export var camera:Camera3D

@export var signals:Array[Event_Token]

func _ready():
	main_cam= get_viewport().get_camera_3d()
	
func _process(delta):
	if Input.is_action_just_pressed("space"):
		test_signal.emit()

func _play():
	if used:
		return
	#if not _has_prerequisite():
	#	return
	Events.emit_signal("on_cutscene_start")
	await _fade_in()
	camera.current = true
	_prepare_Camera(scenes[0])
	await _fade_out()
	
	for scene in scenes:
		_process_scene(scene)
		await Events.on_dialouge_commplete
		
	await _fade_in()
	main_cam.current = true
	await _fade_out()
	
	Events.emit_signal("on_cutscene_end")
	for event_token in signals:
		Events._process_event_token(event_token)
	used = true
	
func _process_scene(scene:Mini_Scene): 
	_prepare_Camera(scene)
	if scene.sfx:
		player.stream = scene.sfx
		player.play()
	if scene.animator:
		print("PLay Animation")
		scene.animator.set(scene.animation_param_path,scene.animation_id)
	if scene.dialouge_id !="":
		Events.emit_signal("on_dialouge", scene.dialouge_name,scene.dialouge_url, Data._load(scene.dialouge_id,scene.dialouge_progress).split(";"))
	#if scene.quest_id !="":
	#	Events.emit_signal("on_quest_progress",scene.quest_id)
		
func _prepare_Camera(scene:Mini_Scene):
	#camera.position = scene.camera_transform.position
	#camera.rotation = scene.camera_transform.rotation
	camera.position = scene.position
	camera.rotation = scene.rotation
	
func _has_prerequisite():
	return false

func _fade_in():
	var tween = create_tween()
	tween.tween_property(fade,"color",Color(0,0,0,1),2).set_ease(Tween.EASE_IN_OUT)
	await tween.finished
	return true
	
func _fade_out():
	var tween = create_tween()
	tween.tween_property(fade,"color",Color(0,0,0,0),2).set_ease(Tween.EASE_IN_OUT).set_delay(1)
	await tween.finished
	return true
	
func _on_area_3d_body_entered(body):
	print(body.name)
	if body.name == "Player":
		if prerequisite !="":
			if not InventoryManager.items.has(prerequisite):
				return
		_play()
