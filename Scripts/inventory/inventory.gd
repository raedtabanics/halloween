extends Control

var item_list:ItemList
@export var texture:Texture2D

var status:bool= false
func _ready():
	Events.on_inventory_update.connect(_update)
	item_list= get_node("NinePatchRect/ItemList")
	_hide()

func _process(delta):
	if Input.is_action_just_pressed("I"):
		if status:
			_hide()
		else:
			_show()
		status = !status
		
func _update():
	item_list.clear()
	for item in InventoryManager.items:
		item_list.add_item(item,texture,true)
	

func _show():
	var tween = create_tween()
	tween.tween_property(self,"scale",Vector2(1,1),.1)

func _hide():
	var tween = create_tween()
	tween.tween_property(self,"scale",Vector2(0,0),.1)
