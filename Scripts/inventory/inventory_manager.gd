extends Node

var items:Dictionary
var first_time_used:bool = true
# Called when the node enters the scene tree for the first time.
func _ready():
	Events.on_item_collected.connect(_add_item)

func _has_item(id):
	if items.has(id):
		return true
	return false
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _add_item(item:ItemModel):
	if not items.has(item._name):
		items[item._name] = item
		Events.emit_signal("on_inventory_update")
	if first_time_used:
		first_time_used = false
		Events.emit_signal("on_tutorial",1)
