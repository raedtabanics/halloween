extends Interactable

@export var parent:Node3D 

@export var offset:float=.2

var original_scale

func _ready():
	original_scale = parent.scale
# Called when the node enters the scene tree for the first time.
func _interact():
	if $Boing_4:
		$Boing_4.play()
	print("Squish")
	var tween = create_tween()
	tween.tween_property(parent,"scale",Vector3(original_scale.x,original_scale.y - offset,original_scale.z),.1)
	tween.tween_property(parent,"scale",Vector3(original_scale.x,original_scale.y + offset,original_scale.z),.05)
	tween.tween_property(parent,"scale",Vector3(original_scale.x,original_scale.y,original_scale.z),.02)
