extends Interactable

@export var id:String
@export var url:String
@export var _name:String

var progress:int = 1

func _interact():
	var raw_msg =Data._load(id,progress)
	var msg = raw_msg.split(";")
	Events.emit_signal("on_dialouge",_name,url,msg)

